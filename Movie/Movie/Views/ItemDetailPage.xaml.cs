﻿using Movie.ViewModels;
using System.ComponentModel;
using Xamarin.Forms;

namespace Movie.Views
{
    public partial class ItemDetailPage : ContentPage
    {
        public ItemDetailPage()
        {
            InitializeComponent();
            BindingContext = new ItemDetailViewModel();
        }
    }
}