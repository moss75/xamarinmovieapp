﻿using Movie.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Movie.Services
{
    public class MockDataStore : IDataStore<Item>
    {
        readonly List<Item> items;

        public MockDataStore()
        {
            items = new List<Item>()
            {
                new Item { Id = Guid.NewGuid().ToString(), Text = "Jean Dujardin", Description="Acteur", Images="JeanDujardin.jpg" },
                new Item { Id = Guid.NewGuid().ToString(), Text = "Omar Sy", Description="Acteur" , Images="OmarSy.jpg"},
                new Item { Id = Guid.NewGuid().ToString(), Text = "Marion Cotillard", Description="Acteur", Images="MarionCotillard.jpg" },
                new Item { Id = Guid.NewGuid().ToString(), Text = "Jean Reno", Description="Acteur", Images="JeanReno.jpg" },
                new Item { Id = Guid.NewGuid().ToString(), Text = "Alexandra Lamy", Description="Acteur", Images="AlexandraLamy.jpg" },
                new Item { Id = Guid.NewGuid().ToString(), Text = "Luc Besson", Description="Réalistaur", Images="LucBesson.jpg" },
                new Item { Id = Guid.NewGuid().ToString(), Text = "Abdellatif Kechiche", Description="Réalistaur", Images="AbdelatifKekiche.jpg" },
                new Item { Id = Guid.NewGuid().ToString(), Text = "Franck Gastambide", Description="Réalistaur", Images="FranckGastambide.jpg" },
                new Item { Id = Guid.NewGuid().ToString(), Text = "Olivier Nakache", Description="Réalistaur", Images="OlivierNakache.jpg" },
                new Item { Id = Guid.NewGuid().ToString(), Text = "Claude Lelouch", Description="Réalistaur", Images="ClaudeLelouch.jpg" },
            };
        }

        public async Task<bool> AddItemAsync(Item item)
        {
            items.Add(item);

            return await Task.FromResult(true);
        }

        public async Task<bool> UpdateItemAsync(Item item)
        {
            var oldItem = items.Where((Item arg) => arg.Id == item.Id).FirstOrDefault();
            items.Remove(oldItem);
            items.Add(item);

            return await Task.FromResult(true);
        }

        public async Task<bool> DeleteItemAsync(string id)
        {
            var oldItem = items.Where((Item arg) => arg.Id == id).FirstOrDefault();
            items.Remove(oldItem);

            return await Task.FromResult(true);
        }

        public async Task<Item> GetItemAsync(string id)
        {
            return await Task.FromResult(items.FirstOrDefault(s => s.Id == id));
        }

        public async Task<IEnumerable<Item>> GetItemsAsync(bool forceRefresh = false)
        {
            return await Task.FromResult(items);
        }
    }
}