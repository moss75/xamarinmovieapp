﻿using Microsoft.EntityFrameworkCore;
using System;
using System.IO;

namespace Movie.DataAcces
{
    public class MovieDbContext : DbContext
    {
        private string v;

        public string DatabasePath { get; private set; }

        public DbSet<Models.Film> Films { get; set; }
        public DbSet<Models.Personne> Personnes { get; set; }
        public DbSet<Models.Image> Images { get; set; }
        public DbSet<Models.LienFilmGenre> LiensFilmsGenres { get; set; }
        public DbSet<Models.LienFilmPersonne> LiensFilmsPersonnes { get; set; }
        
        public DbSet<Models.Genre> Genres { get; set; }

        public MovieDbContext()
            : this(Path.Combine(Environment.GetFolderPath(Environment.SpecialFolder.LocalApplicationData), "movie.db"))
        { }

        public MovieDbContext(string databasePath) : base()
        {
            SQLitePCL.Batteries_V2.Init();
            DatabasePath = databasePath;
        }

        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
            base.OnConfiguring(optionsBuilder);
            optionsBuilder.UseSqlite($"Filename={DatabasePath}");
        }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            base.OnModelCreating(modelBuilder);

            // Définition d'une clé primaire composée
            modelBuilder.Entity<Models.LienFilmGenre>().HasKey(filmgenre => new { filmgenre.IdFilm, filmgenre.IdGenre });
            modelBuilder.Entity<Models.LienFilmPersonne>().HasKey(filmPersonne => new { filmPersonne.IdFilm, filmPersonne.IdPersonne });
            // Définition d'un index unique
            //modelBuilder.Entity<Models.Author>().HasIndex(a => a.LastName).IsUnique();
            // Définition d'une clé étrangère
            //modelBuilder.Entity<Models.Author>().HasMany<Models.Book>().WithOne(b => b.Author).HasForeignKey(b => b.AuthorId);

        }
    }
}
