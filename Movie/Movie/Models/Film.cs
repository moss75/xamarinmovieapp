﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace Movie.Models
{
    public class Film
    {

        [Key, DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int Id { get; set; }
        public string Titre { get; set; }
        public string Synopsys { get; set; }
        public DateTime DateSortie { get; set; }
        public TimeSpan Duree { get; set; }
        public string LienBandeAnnonce { get; set; }
        public string Note { get; set; }C:\Users\shak7\source\repos\Movie

        [InverseProperty(nameof(Image.Film))]
        public ICollection<Image> Images { get; set; }
    }
}
