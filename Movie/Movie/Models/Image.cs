﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace Movie.Models
{
    public class Image
    {
        [Key, DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int Id { get; set; }
        public int IdFilm { get; set; }
        public byte Contenu { get; set; }
        public bool EstImagePrincipale { get; set; }

        [ForeignKey(nameof(IdFilm))]
        public Film Film { get; set; }
    }
}
