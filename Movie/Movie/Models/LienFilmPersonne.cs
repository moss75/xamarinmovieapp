﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Movie.Models
{
    public class LienFilmPersonne
    {
        public int IdFilm { get; set; }
        public int IdPersonne { get; set; }
        public int Role { get; set; }
    }
}
